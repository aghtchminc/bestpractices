package it.euris.patterns.structural.adapter;

public class Main {

    public static void main(String[] args) {
        SocketAdapter socketAdapter = new SocketClassAdapter();
        //SocketAdapter socketAdapter = new SocketObjectAdapter();
        Volt v3 = socketAdapter.get3Volt();
        Volt v12 = socketAdapter.get12Volt();
    }

}
