package it.euris.patterns.behavioral.observer.sample1;

public class Main {

    public static void main(String[] args) {
        NewsAgency ansa = new NewsAgency();
        Observer rai1 = new NewsChannel();
        Observer rai2 = new NewsChannel();

        ansa.register(rai1);
        ansa.register(rai2);
        ansa.releaseNews("italia vince la finale");
    }

}
