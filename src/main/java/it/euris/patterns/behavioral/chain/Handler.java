package it.euris.patterns.behavioral.chain;

public interface Handler {
    void handle(int i);
}

class A implements Handler {

    private Handler handler;

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void handle(int i) {
        if(i < 10){
            System.out.println("A::handle");
        }
        else {
            System.out.println("A::next");
            handler.handle(i);
        }
    }

}

class B implements Handler {

    private Handler handler;

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void handle(int i) {
        if(i < 50){
            System.out.println("B::handle");
        }
        else {
            System.out.println("B::next");
            handler.handle(i);
        }
    }
}

class C implements Handler {

    private Handler handler;

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void handle(int i) {
        System.out.println("C::handle");
    }

}


class Main {

    public static void main(String[] args) {
        A a = new A(); //0
        B b = new B(); //1
        C c = new C(); //2

        // chain of responsibility
        a.setHandler(b);
        b.setHandler(c);

        client(a);
    }

    private static void client(Handler h){
        //h.handle(5);
        //h.handle(15);
        h.handle(55);
    }

}
