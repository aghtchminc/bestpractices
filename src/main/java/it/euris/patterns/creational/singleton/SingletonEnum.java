package it.euris.patterns.creational.singleton;

public enum SingletonEnum {

    INSTANCE;

    private int xyz = 0;

    public void cose(){
        System.out.println("Faccio cose "+xyz);
        xyz += 1;
    }

    public static void main(String[] args) {
        SingletonEnum.INSTANCE.cose();
        SingletonEnum.INSTANCE.cose();
        SingletonEnum.INSTANCE.cose();
    }

}
