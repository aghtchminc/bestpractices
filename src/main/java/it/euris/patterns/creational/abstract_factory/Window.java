package it.euris.patterns.creational.abstract_factory;

public interface Window {
    void show();
}

class WindowsWindow implements Window {
    @Override
    public void show() {
        System.out.println("WIN - Window");
    }
}

class OSXWindow implements Window {
    @Override
    public void show() {
        System.out.println("OSX - Window");
    }
}
