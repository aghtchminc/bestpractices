package it.euris.patterns.behavioral.observer.sample1;


public interface Observer {
    void update();
}

class NewsChannel implements Observer {

    @Override
    public void update() {
        System.out.println("Breaking News available");
    }

}