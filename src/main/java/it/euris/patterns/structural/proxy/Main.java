package it.euris.patterns.structural.proxy;

public class Main {

    public static void main(String[] args) {
        BusinessLogic proxy = new BusinessLogicProxy("user");
        foo(proxy);
    }

    public static void foo(BusinessLogic bl){
        bl.process();
    }

}
