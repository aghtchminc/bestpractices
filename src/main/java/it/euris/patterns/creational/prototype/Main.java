package it.euris.patterns.creational.prototype;

public class Main {

    public static void main(String[] args) {
        Employees e = new Employees();
        e.load();

//        Employees e2 = e;
//        e2.load();

        Employees e3 = (Employees) e.clone();
        e3.load();

        System.out.println(e);
        System.out.println(e3);

    }

}
