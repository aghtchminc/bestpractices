package it.euris.patterns.behavioral.iterator;

public enum ChannelType {
    ENGLISH,
    FRENCH;
}
