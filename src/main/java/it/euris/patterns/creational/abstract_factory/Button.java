package it.euris.patterns.creational.abstract_factory;

public interface Button {
    void show();
}

class WindowsButton implements Button{

    @Override
    public void show() {
        System.out.println("WIN - button");
    }

}

class OSXButton implements Button{

    @Override
    public void show() {
        System.out.println("OSX - button");
    }
}