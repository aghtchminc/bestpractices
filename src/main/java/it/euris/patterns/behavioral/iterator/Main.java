package it.euris.patterns.behavioral.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        ChannelCollection channels = new ChannelCollectionImpl();
        channels.add(new Channel(100.0, ChannelType.FRENCH));  // 0
        channels.add(new Channel(101.0, ChannelType.ENGLISH)); // 1
        channels.add(new Channel(102.0, ChannelType.FRENCH));  // 2
        channels.add(new Channel(103.0, ChannelType.FRENCH));  // 3
        channels.add(new Channel(104.0, ChannelType.ENGLISH)); // 4

        ChannelIterator it = channels.iterator(ChannelType.ENGLISH);
        while (it.hasNext()){
            Channel c = it.next();
            System.out.println(c);
        }

        // In java puro
        List<String> ss = new ArrayList<>();
        ss.add("a");
        ss.add("b");
        ss.add("c");
        Iterator<String> stringIt = ss.iterator();
        while (stringIt.hasNext()){
            System.out.println(stringIt.next());
        }

    }

}
